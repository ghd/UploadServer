﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace ufs.clientDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var filePath = "/Users/loogn/Desktop/aa.pptx";
            var json = UploadFile(filePath).GetAwaiter().GetResult();
            Console.WriteLine(json);
        }


        static async Task<string> UploadFile(string filePath)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("accesstoken", "abcd");
            httpClient.DefaultRequestHeaders.Add("app", "test");

            httpClient.Timeout = TimeSpan.FromMinutes(10);
            var buffer = File.ReadAllBytes(filePath);
            ByteArrayContent byteArray = new ByteArrayContent(buffer);
            byteArray.Headers.Add("ext", Path.GetExtension(filePath));
            var response = await httpClient.PostAsync("http://localhost:5000/uploadfile", byteArray);

            var result = await response.Content.ReadAsStringAsync();
            return result;
        }
    }
}